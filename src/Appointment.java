import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Appointment {
	private static String PATH = "src" + File.separator + "appointments.txt";
	static ArrayList<Appointment> appointmentList = new ArrayList<Appointment>();
	public static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

	protected String customerName;
	protected String staffName;
	protected String type;
	protected Date date;

	public Appointment(String cN, String sN, Date d, String t) {
		this.customerName= cN;
		this.staffName = sN;
		this.date = d;
		this.type = t;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getStaffName() {
		return staffName;
	}

	public Date getDate() {
		return date;
	}

	public String getType() {
		return type;
	}

	/**
	 * Prompts user to enter information to build an appointment object
	 */
	public static void bookAppointment() throws ParseException {
		Scanner s = new Scanner(System.in);
		String res, type = null;

		System.out.println("Please choose your reason for booking appointment");
		System.out.println("1. Loan");
		System.out.println("2. Mortgage");
		System.out.println("3. Credit");
		System.out.print("Selection: ");
		res = s.next();

		switch(res.toLowerCase()) {
		case "1":
			type = "Loan";
			break;
		case "2":
			type = "Mortgage";
			break;
		case "3":
			type = "Credit";
			break;
		}

		System.out.println("When would you like the appointment to take place? (dd/MM/yyyy)");
		res = s.next();

		for(int n = 0; n < Staff.staffList.size(); n++) {
			if (type == Staff.staffList.get(n).getSpecialty()) {
				Date d = df.parse(res); 
				Appointment a = new Appointment(Customer.current.customerName, Staff.staffList.get(n).getUsername(), d, type);

				System.out.println("Creating appointment...");
				appointmentList.add(a);
				break;
			}else {
				System.out.println("Sorry, we are unfortunately too busy to book an appointment at the moment. Please try again later.");
			}
		}
	}

	/**
	 * Lists all previous appointments that have taken place
	 */
	public static void listPrevAppointments() {
		String leftAlignFormat = "| %-10s | %-10s | %-10s | %-10s | %n";

		System.out.format("+------------+------------+------------+------------+%n");
		System.out.format("| Name       | Staff Name | Topic      | Date        %n");
		System.out.format("+------------+------------+------------+------------+%n");
		for (int n = 0; n < appointmentList.size(); n++) {
			System.out.format(leftAlignFormat, appointmentList.get(n).getCustomerName(), appointmentList.get(n).getStaffName(), appointmentList.get(n).getType(), appointmentList.get(n).getDate());
		}
		System.out.format("+------------+------------+------------+------------+%n");
	}

	/**
	 * Loads appointment data from text file for use in the system
	 */
	public static void loadAppointments() throws ParseException, FileNotFoundException {
		File f = new File(PATH).getAbsoluteFile();
		Scanner s = new Scanner (new FileInputStream(f));

		String cN, sN, t, dAS;
		Date parsedDate = new Date();

		for(int n = 0; s.hasNext(); n++) {
			String nextLine = s.nextLine();
			String [] strs = nextLine.split(" ");

			cN = strs[0].toLowerCase();
			sN = strs[1].toLowerCase();
			t = strs[2].toLowerCase();
			dAS = strs[3].toLowerCase();
			parsedDate = df.parse(dAS);
			
			Appointment a = new Appointment(cN, sN, parsedDate, t);
			appointmentList.add(a);
		}
		System.out.println("Appointments successfully imported");
		System.out.println();
		s.close();
	}

	/**
	 * Shows appointments ONLY for the staff member currently logged in
	 */
	public static void viewAppointments() {
		String leftAlignFormat = "| %-10s | %-10s | %-10s | %-10s | %n";

		System.out.format("+------------+------------+------------+------------+%n");
		System.out.format("| Name       | Staff Name | Topic      | Date        %n");
		System.out.format("+------------+------------+------------+------------+%n");
		for (int n = 0; n < appointmentList.size(); n++) {
			if (Staff.loggedIn.getUsername().equals(appointmentList.get(n).getStaffName())) {
				System.out.format(leftAlignFormat, appointmentList.get(n).getCustomerName(), appointmentList.get(n).getStaffName(), appointmentList.get(n).getType(), appointmentList.get(n).getDate());
			}
		}
		System.out.format("+------------+------------+------------+------------+%n");

	}
}
