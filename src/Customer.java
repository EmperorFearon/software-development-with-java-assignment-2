import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

import java.util.Date;
import java.sql.Timestamp;
import java.text.ParseException;


public class Customer {
	private static String PATH = "src" + File.separator + "customers.txt";
	static ArrayList<Customer> customerList = new ArrayList<Customer>();


	static Scanner s = new Scanner(System.in);
	protected String input;
	protected String customerName;
	protected String username;
	protected String password;
	protected String accountNumber;
	protected String Postcode;
	double currentBalance = 5710.87;
	protected float credit;

	public Customer(String cN, String uN, String pW, double cB, float c, String aN, String pC) {
		this.username = uN;
		this.password = pW;
		this.customerName= cN;
		this.accountNumber = aN;
		this.currentBalance = cB;
		this.credit = c;
		this.Postcode= pC;
	}


	public Customer(String uN, String pW) {
		this.username = uN;
		this.password = pW;
	}


	public String getUsername() {
		return username;
	}
	public String getcustomerName() {
		return customerName;
	}
	private String getPassword() {
		return password;
	}

	public String getaccountNumber() {
		return accountNumber;
	}
	public double getcurrentBalance() {
		return currentBalance;
	}

	public float getCredit() {
		return credit;
	}
	public String getPostcode() {
		return Postcode;
	}
	public static void viewInfo() throws ParseException {
		// TODO Auto-generated method stub
		System.out.println("Username:" +current.getUsername());
		System.out.println("Account Number:" +current.getaccountNumber());
		System.out.println("Account Name :"+current.getcustomerName());
		System.out.println("Address :"+current.getPostcode());
		System.out.println("Current Balance :"+current.getcurrentBalance());
		menu();
	}

	public static void viewBalance() throws ParseException {

		System.out.println("Account Number:" +current.getaccountNumber());

		System.out.println("Current Balance :"+current.getcurrentBalance());

		menu();
	}

	public static void viewTransactions(){

		System.out.println("Complete Transactions list");

		System.out.println("\n  Account Number:" +current.getaccountNumber());
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Date	    Details	                Withdrawals	        Deposits	       Balance");
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Apr 8	    Opening Balance			                                       5,234.09");
		System.out.println("Apr 8	    Insurance		                            272.45	           5,506.54");
		System.out.println("Apr 10	    ATM	                                        200.00	           5,306.54");
		System.out.println("Apr 12	    Internet Transfer		250.00	                               5,556.54");
		System.out.println("Apr 12	    Payroll		                                2100.00	           7,656.54");
		System.out.println("Apr 13   	Bill payment	        135.07		                           7,521.47");
		System.out.println("Apr 14   	Direct debit	        200.00		                           7,321.47");
		System.out.println("Apr 14	    Deposit		                                250.00	           7.567.87");
		System.out.println("Apr 15	    Bill payment	        525.72		                           7,042.15");
		System.out.println("Apr 17	    Bill payment	        327.63		                           6,714.52");
		System.out.println("Apr 17	    Bill payment	        729.96		                           5,984.56");
		System.out.println("Apr 18	    Bill payment	        223.69		                           5,710.87");
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("                        Closing Balance			                          $"+current.getcurrentBalance());
	}
	public static void viewRecentTransactions() 
	{
		System.out.println("Recent Transactions");

		System.out.println("\n  Account Number:" +current.getaccountNumber());
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Date	    Details	                Withdrawals	        Deposits	       Balance");
		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("Apr 14	    Deposit		                                250.00	           7.567.87");
		System.out.println("Apr 15	    Bill payment	        525.72		                           7,042.15");
		System.out.println("Apr 17	    Bill payment	        327.63		                           6,714.52");
		System.out.println("Apr 17	    Bill payment	        729.96		                           5,984.56");
		System.out.println("Apr 18	    Bill payment	        223.69		                           5,710.87");

	}
	public static void financialServices() throws ParseException {

		System.out.println(      "Financial Services");	
		System.out.println("---------------------------------");
		System.out.println("1. Apply for loan");
		System.out.println("2. Apply for credit card");
		System.out.println("3. Apply for mortgage");
		System.out.println("4. Back to Menu");

		String input = s.next();

		switch (input.toLowerCase()) {
		case "1":
			System.out.println("Account Number"+current.getaccountNumber()+"\n Loan Application has been sent to the bank");
			break;
		case "2":
			System.out.println("Account Number"+current.getaccountNumber()+"\n Credit card Application has been sent to the bank");
			break;
		case "3":
			System.out.println("Account Number"+current.getaccountNumber()+"\n Mortgage Application has been sent to the bank");
			break;
		case "4":
			s.close();
			Customer.menu();

		default:
			System.out.println("Wrong option - choose an option 1-5");
			break;
		}





	}

	//void dotransaction(float amount,Boolean add)
	//{
	//Date date=  new Date();
	//Timestamp ts =  new Timestamp(date.getTime());
	//File file = null;
	//	FileOutputStream fileOutputStream = null;
	//
	//	String data;
	//	if(add)
	//		data = current.getaccountNumber()+" "+amount+" 0.00"+"\n";
	//	else
	//		data = current.getaccountNumber()+" 0.00 "+amount+"\n";

	//		this.currentBalance=add?(+amount):(-amount);


	//try {
	//	file = new File("transaction.txt").getAbsoluteFile();
	//		fileOutputStream = new FileOutputStream(file);
	//create file if not exists
	//if (!file.exists()) {
	//	file.createNewFile();
	//	}
	//fetch bytes from data
	//	byte[] bs = data.getBytes();
	//fileOutputStream.write(bs);
	//fileOutputStream.flush();
	//fileOutputStream.close();
	//			System.out.println("Transaction done Successfully \n Account number : " + myts);
	//} catch (Exception e) {
	//	e.printStackTrace();
	//	}finally {
	//	try {
	//		if (fileOutputStream != null) {
	//			fileOutputStream.close();
	//		}
	//		} catch (Exception e2) {
	//		e2.printStackTrace();
	//	}
	//	}

	//}

	//	public static void transactions(){
	//
	//		System.out.println("1. Add balance");
	//		System.out.println("2. Send balance");
	//		System.out.println("3. View transactions");
	//		System.out.println("4. Back to Menu");
	//		String input = s.next();
	//
	//		switch (input.toLowerCase()) {
	//		case "1":
	//			Customer.addBalance();
	//			break;
	//		case "2":
	//			Customer.sendBalance();
	//			break;
	//		case "3":
	//			Customer.viewTransactions();
	//			break;
	//
	//		case "4":
	//
	//			menu();
	//			break;
	//		default:
	//			System.out.println("Wrong option - choose an option 1-5");
	//			break;
	//
	//
	//		}
	//	}

	public static void bookAppointment(){
		System.out.println("Book an appointment:");
		System.out.println("--------------------");

		System.out.println("Staff username:");
		String cSelectionAdress = s.next();
		System.out.println("choose a date (dd-mm-yyyy):");
		String cSelectiondate = s.next();
		bookAppointment(cSelectionAdress,cSelectiondate);

	}
	public static void bookAppointment(String staffUsername,String date) {


		File file = null;
		FileOutputStream fileOutputStream = null;

		String data = current.getUsername()+" "+date+"\n";

		try {
			file = new File(staffUsername+"_appointment.txt").getAbsoluteFile();
			fileOutputStream = new FileOutputStream(file,true);
			//create file if not exists
			if (!file.exists()) {
				file.createNewFile();
			}
			//fetch bytes from data
			byte[] bs = data.getBytes();
			fileOutputStream.write(bs);
			fileOutputStream.flush();
			fileOutputStream.close();
			System.out.println("Appointment added Successfully for date:"+date);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}


		// TODO Auto-generated method stub
	}

	public static void editProfile() throws ParseException {
		System.out.println("Edit Profile Enter your details below:");
		System.out.println("--------------------------------------");
		System.out.println("Name:");
		String cSelectionname = s.next();
		System.out.println("Address (Postcode):");
		String cSelectionAdress = s.next();
		System.out.println("Username:");
		String _username_ = s.next();
		System.out.println("Password:");
		String _password_ = s.next();
		Customer.savechanges(cSelectionname, _username_, _password_, cSelectionAdress);
		try{Customer.importCustomers();}catch(Exception e){}
		menu();

		return;
	}


	public static void savechanges(String customerName,String username,String password,String Postcode)
	{
		File file = null;
		FileOutputStream fileOutputStream = null;

		Customer editcustomer=new Customer(customerName,username,password,0,0,current.getaccountNumber(),Postcode);
		customerList.set(customerList.indexOf(current),editcustomer);

		String saveachanges="";

		for(Customer cs: customerList) {
			saveachanges += cs.getcustomerName()+" "+cs.getUsername()+" "+cs.getPassword()+" 0.00"+" 0.00 "+cs.getaccountNumber()+" "+cs.getPostcode()+"\n";
		}

		try {
			file = new File(PATH).getAbsoluteFile();
			fileOutputStream = new FileOutputStream(file);
			//create file if not exists
			if (!file.exists()) {
				file.createNewFile();
			}
			//fetch bytes from data
			byte[] bs = saveachanges.getBytes();
			fileOutputStream.write(bs);
			fileOutputStream.flush();
			fileOutputStream.close();
			System.out.println("Cutomer details edited Successfully \n Account number : "+current.getaccountNumber());

		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}

	}



	public static void importCustomers() throws Exception {
		File f = new File(PATH).getAbsoluteFile();
		Scanner s = new Scanner (new FileInputStream(f));

		String uN, pW, cN, pC;
		String aN;
		double cB;
		float  c;

		for(int n=0; s.hasNext(); n++) {
			String nextLine = s.nextLine();
			String [] strs = nextLine.split(" ");
			cN = strs[0];
			uN = strs[1];
			pW = strs[2];
			cB = Double.parseDouble(strs[3]);
			c = Float.parseFloat(strs[4]);
			aN = strs[5];
			pC= strs[6];
			Customer u = new Customer(cN, uN, pW, cB, c, aN, pC);
			customerList.add(u);
		}
		System.out.println("Customers successfully imported");
		s.close();
	}


	static Customer current;
	public static void login() throws ParseException{
		boolean userFound = false;

		do {
			System.out.print("Username: ");
			String username = s.next();

			System.out.print("Password: ");
			String password = s.next();

			for (int n = 0; n < customerList.size(); n++) {
				if (username.equals(customerList.get(n).getUsername()) || password.equals(customerList.get(n).getPassword())) {
					current = customerList.get(n);
					System.out.println("Logged in..."+"\n Name :"+customerList.get(n).getcustomerName()+"\n Account number :"+customerList.get(n).getaccountNumber());
					userFound = true;
					break;
				}
			}
			if(!userFound) {
				System.out.println("Error logging in. Please try again");
				main.findUserType();
			}
		}while(!userFound);
	}


	public static void menu() throws ParseException{
		System.out.println("      -- Customer Menu --");
		System.out.println("---------------------------------");
		System.out.println("1. View Account Information");
		System.out.println("2. View Account Balance");
		System.out.println("3. View Transactions");
		System.out.println("4. Add/change personal details");
		System.out.println("5. Recent transactions");
		System.out.println("6. Financial services"); 
		System.out.println("7. Book Appointment");
		System.out.println("8. LogOut");

		String input = s.next();

		switch (input.toLowerCase()) {
		case "1":
			Customer.viewInfo();
			break;
		case "2":
			Customer.viewBalance();
			break;
		case "3":
			Customer.viewTransactions();
			menu();
			break;
		case "4":
			Customer.editProfile();
			break;
		case "5":
			Customer.viewRecentTransactions();
			menu();
			break;
		case "6":
			Customer.financialServices();
			menu();
			break;
		case "7":
			Customer.bookAppointment();
			menu();
			break;
		case "8":

			main.findUserType();
		default:
			System.out.println("Wrong option - choose an option 1-8");
			break;
		}

	}

	public static void newCustomer(Customer c ) {
		customerList.add(c);
	}
}
