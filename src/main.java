import java.text.ParseException;
import java.util.Scanner;

public class main {

	protected static Scanner s = new Scanner(System.in);

	public static void main(String[] args) throws Exception {
		Customer.importCustomers();								//Importing data from text files
		Staff.importStaff();
		Appointment.loadAppointments();

		findUserType();											//Find out if user is a manager and delivers menu based on this
	}

	/**
	 * Prompts user to specify what type of user they are and navigates them to their part of system
	 * @throws ParseException
	 */
	public static void findUserType() throws ParseException{
		boolean flag = false;

		System.out.println("== BANK SYSTEM ==");

		while(!flag) {
			System.out.println("Welcome. Are you a customer or staff memeber? ");
			System.out.println("1. Customer");
			System.out.println("2. Staff Member");
			System.out.println("Q. End Session");
			System.out.print("Selection: ");
			String selection = s.next();

			switch(selection.toLowerCase()) {
			case "1":
				flag = true;
				Customer.login();													//Login method using customer data
				Customer.menu();
				break;
			case "2":
				flag = true;
				Staff.login();														//Login method using staff data
				break;
			case "q":
				System.exit(0);														//Terminates program
			default:
				System.out.println("Invalid entry. Please try again...");			//Validation of user input
				break;
			}
		}
	}
}

