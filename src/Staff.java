import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Staff {
	private static String PATH = "src" + File.separator + "staff.txt";
	static ArrayList<Staff> staffList = new ArrayList<Staff>();
	static Staff loggedIn = new Staff();

	protected String username;
	protected String password;
	protected int userLevel;
	protected String speciality;
	protected String workingHours;

	public Staff(String uN, String pW, int uL, String s, String wH) {					//Main constructor for staff
		this.username = uN;
		this.password = pW;
		this.userLevel = uL;
		this.speciality = s;
		this.workingHours = wH;
	}

	public Staff() {																	//Basic constructor for quick declaration

	}

	/**
	 * Returns the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Returns the password. Only to be used in Staff class for security
	 */
	protected String getPassword() {
		return password;
	}

	/**
	 * Returns if the user is a manager or not
	 */
	public int getUserLevel() {
		return userLevel;
	}

	/**
	 * Returns the specialty of the staff member
	 */
	public String getSpecialty() {
		return speciality;
	}

	/**
	 * Returns the working hours of the staff member
	 */
	public String getWorkingHours() {
		return workingHours;

	}

	/**
	 * Refers the appointment to a manager
	 */
	public static void referToManager() {
		// TODO Auto-generated method stub

	}

	/**
	 * Returns the manager value in a readable format
	 */
	public static String parseManager(Staff s) {
		String output;

		if(s.getUserLevel() == 1) {
			output = "Yes";
		}else {
			output = "No";
		}

		return output;
	}

	/**
	 * Shows information of an existing account
	 */
	public static void viewAccount() {
		Scanner s = new Scanner(System.in);

		System.out.print("Please enter the username of the account: ");
		String res = s.next();

		for(int n = 0; n < Customer.customerList.size(); n++) {
			if (Customer.customerList.get(n).getUsername() == res) {
				String leftAlignFormat = "| %-10s | %-10s | %n";

				System.out.format("+------------+------------+------------+%n");
				System.out.format("| Name       | Balance    | Credit     +%n");
				System.out.format("+------------+------------+------------+%n");
				System.out.format(leftAlignFormat, Customer.customerList.get(n).getUsername(), Customer.customerList.get(n).getCredit());
				System.out.format("+------------+------------+------------+%n");
			}
		}
		s.close();
	}

	/**
	 * Shows pending applications for evaluation
	 */
	public static void viewCurrentApplications() {
		// TODO Auto-generated method stub

	}

	/**
	 * Returns a list of staff memebers from the staffList
	 */
	public static void listStaff() {
		String leftAlignFormat = "| %-10s | %-10s | %n";

		System.out.format("+------------+------------+%n");
		System.out.format("| Name       | Specialty   %n");
		System.out.format("+------------+------------+%n");
		for (int n = 0; n < staffList.size(); n++) {
			System.out.format(leftAlignFormat, staffList.get(n).getUsername(), staffList.get(n).getSpecialty());
		}
		System.out.format("+------------+------------+%n");

	}

	/**
	 * Creates a new user on the system (Can be either staff or customer)
	 */
	public static void addAccount() {
		Scanner s = new Scanner(System.in);
		boolean staff = false;
		String sel;

		System.out.println("Is user a customer or staff member?");
		System.out.println("1. Customer");
		System.out.println("2. Staff Member");
		System.out.print("Selection: ");
		sel = s.next();

		switch(sel) {
		case "1":
			break;
		case "2":
			staff = true;
			break;
		}

		System.out.print("Please enter username: ");
		String username = s.next();

		System.out.print("Please enter users password: ");
		String password = s.next();

		if (staff) {
			System.out.println("What is " + username + "'s specialty?: ");
			String speciality = s.next();

			System.out.println("What is " + username + "'s working hours? (Example: 9-5)");
			String workingHours = s.next();

			Staff S = new Staff(username, password, 0, speciality, workingHours);
			staffList.add(S);
		}else{
			Customer c = new Customer(username, password);
			Customer.newCustomer(c);
		}
		s.close();
	}

	/**
	 * Removes user from the arraylist (Can be either staff or customer)
	 */
	public static void removeAccount() {
		Scanner s = new Scanner(System.in);

		System.out.print("Please enter username of account: ");
		String username = s.next();

		boolean userFound = false;

		for (int n = 0; n < staffList.size(); n++) {
			if (staffList.get(n).getUsername() == username) {
				userFound = true;
				System.out.println("User found! Removing account...");
				staffList.remove(n);
				break;
			}
		}if(!userFound) {
			for (int n = 0; n < Customer.customerList.size(); n++) {
				if (Customer.customerList.get(n).getUsername() == username) {
					userFound = true;
					System.out.println("User found! Removing account...");
					Customer.customerList.remove(n);
					break;
				}
			}
		}
		s.close();
	}

	public static void reviewApplications() {
		// TODO Auto-generated method stub

	}

	/**
	 * Returns information about the current staff user
	 */
	public static void viewInfo() {
		String leftAlignFormat = "| %-10s | %-10s | %-10s | %n";

		System.out.format("+------------+------------+------------+%n");
		System.out.format("| Name       | Specialty  | Manager    +%n");
		System.out.format("+------------+------------+------------+%n");
		System.out.format(leftAlignFormat, loggedIn.getUsername(), loggedIn.getSpecialty(), parseManager(loggedIn));

		System.out.format("+------------+------------+------------+%n");
	}

	/**
	 * Returns the working hours of the current staff user
	 */
	private static void printWorkingHours() {
		// TODO Auto-generated method stub

	}

	/**
	 * Menu delivered when staff user is NOT manager
	 */
	public static void staffMenu() {
		Scanner s = new Scanner(System.in);
		boolean flag = false;
		String res;

		do{
			System.out.println("-- Main Menu --");
			System.out.println("1. View Employee Information");
			System.out.println("2. View Working Hours");
			System.out.println("3. View Appointments");
			System.out.println("4. View Previous Appointments");
			System.out.println("5. Refer to Manager");
			System.out.println("6. View Account");
			System.out.println("7. List Staff Members");
			System.out.println("Q. Exit");

			System.out.print("Selection: ");
			res = s.next();

			switch(res.toLowerCase()) {
			case "1":
				viewInfo();
				break;
			case "2":
				printWorkingHours();
				break;
			case "3":
				Appointment.viewAppointments();
				break;
			case "4":
				Appointment.listPrevAppointments();
				break;
			case "5":
				referToManager();
				break;
			case "6":
				viewAccount();
				break;
			case "7":
				listStaff();
				break;
			case "q":
				System.exit(0);
			}
		}while(!flag);
		s.close();
	}

	/**
	 * Menu delivered when staff user IS manager
	 */
	public static void managerMenu() throws ParseException {
		Scanner s = new Scanner(System.in);
		String input = null;

		while(input != "q") {
			System.out.println("-- Main Menu --");
			System.out.println("1. Add Account");
			System.out.println("2. Remove Account");
			System.out.println("3. Review Applications");
			System.out.println("4. Book Appointment");
			System.out.println("Q. Exit");

			input = s.next();

			switch (input.toLowerCase()) {
			case "1":
				addAccount();
				break;
			case "2":
				removeAccount();
				break;
			case "3":
				reviewApplications();
				break;
			case "4":
				Appointment.bookAppointment();
				break;
			case "q":
				s.close();
				System.exit(0);
			}
		};
	}

	/**
	 * Loads staff data from text file for use in the system
	 */
	public static void importStaff() throws FileNotFoundException {
		File f = new File(PATH).getAbsoluteFile();
		Scanner s = new Scanner (new FileInputStream(f));

		String uN, pW, sp, wH;
		int uL;

		for(int n = 0; s.hasNext(); n++) {
			String nextLine = s.nextLine();
			String [] strs = nextLine.split(" ");

			uN = strs[0].toLowerCase();
			pW = strs[1].toLowerCase();
			uL = Integer.parseInt(strs[2]);
			sp = strs[3].toLowerCase();
			wH = strs[4].toLowerCase();

			Staff u = new Staff(uN, pW, uL, sp, wH);
			staffList.add(u);
		}
		System.out.println("Staff successfully imported");
		s.close();

	}

	/**
	 * Prompts staff user to login
	 */
	public static void login() throws ParseException {
		Scanner s = new Scanner(System.in);
		boolean userFound = false;

		do {
			System.out.print("Username: ");
			String username = s.next().toLowerCase();

			System.out.print("Password: ");
			String password = s.next().toLowerCase();

			for (int n = 0; n < staffList.size(); n++) {
				staffList.get(n);
				String uN = staffList.get(n).getUsername();
				String pW = staffList.get(n).getPassword();

				if (username.equals(uN) && password.equals(pW)) {
					loggedIn = new Staff(staffList.get(n).getUsername(), staffList.get(n).getPassword(), staffList.get(n).getUserLevel(), staffList.get(n).getSpecialty(), staffList.get(n).getWorkingHours());
					System.out.println("Logged in...");
					int m = staffList.get(n).getUserLevel();
					if(m == 1) {
						managerMenu();
					}if(m == 0) {
						staffMenu();
					}

					userFound = true;
					break;
				}
			}
			if(!userFound) {
				System.out.println("Error logging in. Please try again");
			}
		}while(!userFound);
		s.close();
	}
}
